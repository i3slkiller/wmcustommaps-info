# Abandoned Village 2019 (Opuszczona wioska)
* pierwsze wydanie: 2019-07-03
* ostatnia aktualizacja: 2020-03-29
* dozwolone bronie: wszystkie
* dodatkowe informacje: Ta mapa może ładować się góra 2 minuty, więc poczekaj cierpliwie. Zastanawiam się, jak tą mapę zoptymalizować. Broń w magazynie pojawi się w następnej aktualizacji.

## Wersja 2 (2019-07-08)
* teraz nie masz już możliwości wyjścia poza mapę
* dodano nazwy i krótkie teksty graczy TERMINATOR1000 i donghominhanh123 w domach
* dodano więcej punktów odrodzenia
* naprawiono trochę drzew
* nieco przesunięto kamerę startową

## Wersja 3 (2019-08-16)
* dodano nazwę gracza xXkillerrXx i obrazek przedstawiający go
* naprawiono wieżę
* dodano kilka broni UMP-45 w składzie broni
* małe poprawki błędów

## Wersja 3(.1) (2019-08-16)
* beczki zostały zastąpione bramkami na boisku

## Wersja 4 (2020-03-26)
* dodano drzwi w domu
* dodano nowe bronie i skrzynie w magazynie

## Wersja 4(.1) (2020-03-29)
* dodano dwa czołgi