# Winter 3 2019
* first release: 2019-09-04
* last update: 2022-02-09
* allowed weapons: all
* additional info: none

## Version 2 (2020-03-26)
* slightly lowered bed in the house
* added new campfire
* I also hung a picture of oskar100c on the wall

## Version 2(.1) (2020-04-02)
* removed oskar100c's picture (he is suspected of cheating, and I'm not going to portray them on my maps)
* added xXkillerrXx's picture in the tent

## Version 3 (2022-02-09)
* resized TriggerDamage to fill whole water