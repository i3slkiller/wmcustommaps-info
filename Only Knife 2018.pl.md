# Only Knife 2018 (Walka tylko na kosy)
* pierwsze wydanie: 2018-09-22
* ostatnia aktualizacja: 2018-11-03
* dozwolone bronie: Knife, BrassKnuckless, Katana
* dodatkowe informacje: Please don't use any weapons except Knife, Katana and BrassKnuckless on this map because it will be disabled!
* pierwsze wyłączenie: 2019-02-27 (gracze nie grali uczciwie na tej mapie (woleli napierdalać karabinami i snajperkami zamiast kosami), wykorzystywali ją do farmienia XP, mogę zrobić prywatny serwer z tą mapą)
* ponowne wydanie: 2019-10-15
* ostatnie wyłączenie: 2019-10-14

## Wersja 2 (2018-10-12)
* poprawiono spawnpointy

## Wersja 3 (2018-10-13)
* naprawiono błąd "migotania" ścian przy wyjściu ze spawnu

## Wersja 4 (2018-11-03)
* dodano "ruchomą podłogę" na korytarzu prowadzącym do areny