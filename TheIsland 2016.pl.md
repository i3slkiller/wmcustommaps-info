# TheIsland 2016 (Wyspa)
* pierwsze wydanie: 2016 rok
* ponowne dodanie: 2018-10-03
* ostatnia aktualizacja: 2022-05-02
* dozwolone bronie: wszystkie
* dodatkowe informacje: brak
* pierwsze wyłączenie: 2022-02-02
* ponowne wydanie: 2022-04-04
* ostatnie wyłączenie: 2022-11-13

## Wersja 2 (2018-10-20)
* drabina jest teraz bliżej wieży w pobliżu punktów odradzania się niebieskich

## Wersja 3 (2022-05-02)
* dodano więcej TriggerDamagów pod wodą w celu zapobiegnięcia wejściu tam nawet wtedy, gdy jeden z nich nie zabije gracza