# Containers 2020 (Kontenery)
* pierwsze wydanie: 2020-04-07
* ostatnia aktualizacja: 2020-12-31
* dozwolone bronie: wszystkie
* dodatkowe informacje: brak

## Wersja 2 (2020-04-10)
* zoptymalizowano kamery
* przeniesiono telewizor pokazujący spawn niebieskich, na jego miejsce postawiono wyłączony telewizor
* teraz jak spróbujesz przejść przez bramę, zostaniesz natychmiast zabity

## Wersja 3 (2020-12-31)
* zastąpiono kontener z oknem tym zrobionym przeze mnie

## Wersja 4 (2022-11-12)
* wypalono cienie i dodano sondy świetlne
* zastąpiono tablice z napisami w ramkach wykonane z kostek tymi zrobionymi w Probuilderze
* dodano zamknięcia i białe światła w kontenerach z czołgiem i z napisem

### Uwagi
* W niektórych miejscach moża zauważyć, że ruchomy obiekt jest zbyt jasny jak na otoczenie, w którym przebywa.
* Dodanie światła w w/w kontenerach jest obejściem problemu, dla którego obecnie szukam rozwiązania. Objawia się zbyt jasnymi ruchomymi obiektami w ciemnych miejscach mimo ułożenia tu sond świetlnych.
* Może to być spowodowane istnieniem oddzielnych grup sond świetnych dla każdego obiektu (1 dla terenu, 1 dla jednego kontenera, 1 dla jednej ściany, 2 dla powietrza), lecz muszę sprawdzić, czy rzeczywiście z tego powodu tak się dzieje.