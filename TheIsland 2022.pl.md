# TheIsland 2022
* pierwsze wydanie: 2022-11-30
* dozwolone bronie: wszystkie
* dodatkowe informacje: Ta mapa jest modyfikacją wycofanej mapy 'TheIsland 2016' wydanej (jak sama nazwa wskazuje) w połowie 2016 roku. Zdecydowałem się ją wydać jako nową mapę, bo wprowadza nowe rzeczy, których wtedy nie było
* uwagi: 
    - Ta mapa z wypalonymi cieniami i sondami świetlnymi wyeksportowana w najnowszej wersji eksportera (1.0.4) zajmuje ponad 100 MiB, nawet gdy usunę dodane wcześniej sondy świetlne, odznaczę fajkę 'Static' ze wszystkich obiektów i "wypalę" cienie (które ważą 0 bajtów). Nie wiem, od czego to zależy, tym bardziej że inna mapa w innym projekcie po wypaleniu cieni na mapie razem z sondami świetlnymi skopiowanymi z tej mapy zajmowała poniżej 10 MiB (wymaga dalszego sprawdzenia). Po cofnięciu do wersji 1.0.3 dokładnie ta sama mapa zajmuje ~9.5 MiB (w tym wypalone cienie ~4 MiB). Oczywiście sondy świetlne wtedy nie będą działać, efektem tego będą jasne ruchome obiekty w ciemnym miejscu, np. tunelu.
    - Zauważyłem, że na tej mapie w grze nie widać, by były wypalone cienie na terenie (choć w edytorze wyraźnie widać, że są). Teren ma wymiary 1024x1024, a tekstura wypalonych cieni na nim 512x512.
    - Istnieje możliwość wypadnięcia z latającego spodka.
    - Ta mapa wymaga optymalizacji w celu zwiększenia wydajności, mogą w tym pomóc komponent LODGroup, (który działa od wersji 1.0.4) i mniej szczegółowe obiekty.
    - Tymczasowo usunąłem drzwi garażowe w bazach z powodu pojawiania się ich w pobliżu miejsca docelowego, gdy punkty startowe samochodów stały zbyt blisko czujnika otwarcia {Box Collider} (wymaga dalszego sprawdzenia). To samo można zaobserwować na innej mojej mapie, mimo że wyeksportowana starym eksporterem (czyli nie z gita).

## Wersja 2 (2022-12-15)
* poprawiono nieco wydajność poprzez usunięcie podobnych, identycznych i niepotrzebnych materiałów
* skrócono czas postoju latającego spodka do 15 sekund
* nieco poprawiono tor lotu latającego spodka
* próbowano poprawić wypalone cienie w "rurach" budynku na środku
* powiększono kładki i zwiększono wysokość osłon wokół nich na dwóch wieżach
* zwiększono wysokość większości drewnianych osłon
* poprawiono modele drabin, stołów niebieskich, lampy niebieskich, hali niebieskich, budynku bazy niebieskich i wieży przy kryjówce 2
* cienie są już wypalane na drabinach statku