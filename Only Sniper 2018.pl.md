# Only Sniper 2018 (Walka tylko na snajperki)
* pierwsze wydanie: 2018-09-26
* ostatnia aktualizacja: 2019-01-01
* dozwolone bronie: L115A3, PlasmaGun, RailRifle, USP, PulsePistol, Revolver, WarriorBow, TrapMine, Knife, BrassKnuckless, Katana
* dodatkowe informacje: brak

## Wersja 2 (2019-01-01)
* wyjścia z punktów odradzania są teraz szersze
* nieco przesunięto skrzynie i zwiększono szerokość osłony wyjść z punktów odradzania