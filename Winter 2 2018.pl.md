# Winter 2 2018 (Zima 2)
* pierwsze wydanie: 2018-09-29
* ostatnia aktualizacja: 2019-02-22
* dozwolone bronie: wszystkie
* dodatkowe informacje: brak

## Wersja 2 (2018-10-20)
* dodano kolizje na antenie na ścianie domu (czytaj: nie możesz strzelać ani przenikać przez antenę)
* nieco poprawiono schody

## Wersja 3 (2019-02-22)
* dodano lekką mgłę w lesie
* naprawiono błąd z dwoma latającymi drzewami w lesie