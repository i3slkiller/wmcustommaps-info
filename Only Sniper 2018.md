# Only Sniper 2018
* first release: 2018-09-26
* last update: 2019-01-01
* allowed weapons: L115A3, PlasmaGun, RailRifle, USP, PulsePistol, Revolver, WarriorBow, TrapMine, Knife, BrassKnuckless, Katana
* additional info: none

## Version 2 (2019-01-01)
* entress of the spawns is now wider
* slightly moved boxes and increased width cover of the entress