# Only Knife 2018
* first release: 2018-09-22
* last update: 2018-11-03
* allowed weapons: Knife, BrassKnuckless, Katana
* additional info: Please don't use any weapons except Knife, Katana and BrassKnuckless on this map because it will be disabled!
* first disable: 2019-02-27 (players didn't play fair on this map, they used this map for XP farming, but I can make private server with this map)
* again published: 2019-10-15
* last disable: 2019-10-14

## Version 2 (2018-10-12)
* improved spawns

## Version 3 (2018-10-13)
* fixed "flickering" bug at exit from spawns

## Version 4 (2018-11-03)
* added "moving floor" in the corridor leading to the arena