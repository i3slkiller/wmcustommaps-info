# TheIsland 2016
* first release: 2016
* upload date: 2018-10-03
* last update: 2022-05-02
* allowed weapons: all
* additional info: none
* first disable: 2022-02-02
* again published: 2022-04-04
* last disable: 2022-11-13

## Version 2 (2018-10-20)
* the ladder is now closer to the tower near the blue spawns

## Version 3 (2022-05-02)
* added more TriggerDamages under water to prevent going here, even if one of them won't kill player