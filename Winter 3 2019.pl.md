# Winter 3 2019 (Zima 3)
* pierwsze wydanie: 2019-09-04
* ostatnia aktualizacja: 2022-02-09
* dozwolone bronie: wszystkie
* dodatkowe informacje: brak

## Wersja 2 (2020-03-26)
* nieco obniżono łóżko w domu
* dodano nowe ognisko
* powiesiłem też na ścianie obraz oskara100c (niech teraz mnie doda na swojej mapie xD)

## Wersja 2(.1) (2020-04-02)
* skasowano obrazek oskara100c (jest podejrzewany o oszukiwanie, a ja nie zamierzam przedstawiać takich na moich mapach, zresztą wiedziałem że tak się to skończy)
* dodano obrazek xXkillerrXx'a do namiotu

## Wersja 3 (2022-02-09)
* zmieniono rozmiar TriggerDamage w celu wypełnienia całej wody