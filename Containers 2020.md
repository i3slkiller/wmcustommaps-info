# Containers 2020
* first release: 2020-04-07
* last update: 2020-12-31
* allowed weapons: all
* additional info: none

## Version 2 (2020-04-10)
* optimized cameras
* moved TV showing blue spawns, in its place putted a turned off TV
* now if you try to go through the gate, you will be killed immediately

## Version 3 (2020-12-31)
* replaced container with window for one made by myself

## Version 4 (2022-11-12)
* baked lightmaps and added light probes
* replaced inscription boards in frames made using cubes with ones made in Probuilder
* added closures and white lights in containers with tank and inscription

### Notes
* In some places you may notice that moving object is too bright for its surroundings.
* Adding light in the above-mentioned containers is workaround of the problem, for which I'm currently looking solution. It manifest itself with too bright moving objects in dark places even if light probes are here.
* It may be caused by existence of separate light probe groups for each object (1 for terrain, 1 for one container, 1 for one wall, 2 for air), but I need to check whether this is indeed case for this reason.