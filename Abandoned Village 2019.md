# Abandoned Village 2019
* first release: 2019-07-03
* last update: 2020-03-29
* allowed weapons: all
* additional info: This map can load up to 2 minutes, so please be patient. I wonder how to optimize this map. Weapons in the warehouse will be in next update.

## Version 2 (2019-07-08)
* now you don't have ability to go out of the map
* added TERMINATOR1000's and donghominhanh123's names and their short texts in the houses
* added more spawnpoints
* fixed some trees
* slightly moved start camera

## Version 3 (2019-08-16)
* added xXkillerrXx's name and picture representing him
* fixed tower
* added few UMP-45's in the warehouse
* small bug fixes

## Version 3(.1) (2019-08-16)
* barrels were replaced with gates on the pitch

## Version 4 (2020-03-26)
* added a door to the house
* added new weapons and crates in the warehouse

## Version 4(.1) (2020-03-29)
* added two tanks