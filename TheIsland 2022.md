# TheIsland 2022
* first release: 2022-11-30
* allowed weapons: all
* additional info: This map is modification of deprecated 'TheIsland 2016' map released in year 2016 (as name suggests). I decided release it as new map, because introduce new things nonexistent at this time
* notes: 
    - This map with baked lightmaps and light probes after exporting in the latest version of the exporter (1.0.4) takes over 100 MiB, even when I remove previously added light probes, uncheck 'Static' checkbox from all objects and "bake" lightmaps (which takes 0 bytes). I don't know what it depends on, especially that other map in other project after baking lightmaps on the map together with light probes copied from this map took less than 10 MiB (needs further checking). After downgrading exporter to version 1.0.3 exactly the same map take ~9.5 MiB (including baked lightmaps ~4 MiB). Obviously, then light probes won't work, which result in bright moving objects in dark places, e.g. in tunnel.
    - I noticed that on this map in the game that baked lightmaps on terrain are not visible (although in the editor there are clearly visible). Terrain is 1024x1024 and baked lightmaps texture on that 512x512.
    - There is a possibility of falling out of the flying UFO.
    - This map need optimization for better performance, LODGroup component (which work since 1.0.4) and less detailed objects may help with it.
    - Temporarily removed garage doors in bases because of appearing them near their destination when CarPoints were too close opening sensor {Box Collider} (requires further checking). The same can be seen on other my map, even though exported with older exporter (not from git).

## Version 2 (2022-12-15)
* slightly improved performance by removing similar, identical and unnecessary materials
* shortened downtime of UFO to 15 seconds
* slightly corrected flight path of UFO
* tried to fix baked lightmaps on "pipes" in building on the middle
* enlarged tiles and increased height of the shields around them on two towers
* increased height of most of the wooden shields
* fixed models of ladders, blue tables, blue lamps, blue hall, blue base building and tower near hideout 2
* shadows are now baked on ship ladders