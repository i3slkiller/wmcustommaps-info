# Winter 2 2018
* first release: 2018-09-29
* last update: 2019-02-22
* allowed weapons: all
* additional info: none

## Version 2 (2018-10-20)
* added collisions on the antenna on the wall of the house
* slightly corrected stairs

## Version 3 (2019-02-22)
* added light fog in the forest
* fixed bug with 2 flying trees in the forest